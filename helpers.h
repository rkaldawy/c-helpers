#ifndef HELPERS_H
#define HELPERS_H

#include <pthread.h>
#include <sempahore.h>

typedef struct Queue{
  void* data[MAX_QUEUE_SIZE];
  int head;
  int tail;
  sem_t fullSem;
  sem_t emptySem;
  pthread_mutex_t dataLock;
} Queue;

//initialize a queue
void initQueue(Queue* queue);

//add an element to the queue
void enqueue(Queue* queue, void* data);

//remove an element from the queue
void* dequeue(Queue* queue);

//peek the top queue element
void* peek(Queue* queue);

#endif
