#include <pthread.h>
#include <semaphore.h>
#include "helpers.h"

void initQueue(Queue* queue)
{
  queue->head = 0;
  queue->tail = 0;
  sem_init(&queue->fullSem, 0, MAX_QUEUE_SIZE-1);
  sem_init(&queue->emptySem, 0, 0);
}

void enqueue(Queue* queue, void* data)
{
  sem_wait(&queue->fullSem);
  pthread_mutex_lock(&queue->dataLock);
  queue->data[queue->head] = data;
  if(queue->head == MAX_QUEUE_SIZE-1){
    queue->head = 0;
  }
  else {
    queue->head++;
  }
  pthread_mutex_unlock(&queue->dataLock);
  sem_post(&queue->emptySem);
}

void* dequeue(Queue* queue)
{
  sem_wait(&queue->emptySem);
  pthread_mutex_lock(&queue->dataLock);
  void* item = queue->data[queue->tail];
  if(queue->tail == MAX_QUEUE_SIZE-1){
    queue->tail = 0;
  }
  else{
    queue->tail++;
  }
  pthread_mutex_unlock(&queue->dataLock);
  sem_post(&queue->fullSem);

  return item;
}

void* peek(Queue* queue)
{
  pthread_mutex_lock(&queue->dataLock);
  void* item = queue->data[queue->tail];
  pthread_mutex_unlock(&queue->dataLock);
  
  return item;
}
